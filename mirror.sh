#!/bin/sh -e

urweb blog -output blog-web.exe 
./blog-web.exe & echo $! >blog.pid
wget -m localhost:8080/index.html
kill -9 $(cat blog.pid)
mv localhost:8080 public 
rm -rf blog.pid
